(function () {
    'use strict';
    var gulp = require('gulp');
    var $ = require('gulp-load-plugins')();
    var rimraf = require('rimraf');
    var lazypipe = require('lazypipe');
    var wiredep = require('wiredep').stream;
    var runSequence = require('run-sequence');
    var sass = require('gulp-sass');
    var paths = {
        scripts: 'app/js/**.js',
        css: 'app/css/**.css',
        images: ['app/images/**/*'],
        html: ['app/**.html'],
        font: 'app/fonts/*',
        icons: 'app/icons/*',
        sitemap: 'app/sitemap.xml',
        robots: 'app/robots.txt',
        profile: 'App_Data/**/*',
        sass: 'app/sass/main.scss',
        videos: 'app/videos',
        views:'app/views/*.html'
    }
    var dist = {
        main: 'dist',
        scripts: 'dist',
        css: 'dist',
        images: 'dist/images',
        font: 'dist/fonts',
        icons: 'dist/icons',
        profile: 'dist/App_Data',
        videos: 'dist/videos',
        views: 'dist/views'
    }

    ////////////////////////
    // Reusable pipelines //
    ////////////////////////

    var lintScripts = lazypipe()
      .pipe($.jshint, '.jshintrc')
      .pipe($.jshint.reporter, 'jshint-stylish');

    var styles = lazypipe()
      .pipe($.autoprefixer, 'last 1 version')
      .pipe(gulp.dest, '.tmp/styles');

    gulp.task('client:build', function () {
        var jsFilter = $.filter(paths.scripts);
        var cssFilter = $.filter(paths.css);
        return gulp.src(paths.html)
            .pipe($.plumber())
            .pipe($.useref())
             .pipe(jsFilter)
             .pipe($.uglify())
             .pipe(jsFilter.restore())
             .pipe(cssFilter)
             .pipe($.minifyCss({ cache: true }))
             .pipe(cssFilter.restore())
             //.pipe($.rev())
             //.pipe($.revReplace())
             .pipe(gulp.dest(dist.main))
    });

    gulp.task('images', function () {
        return gulp.src(paths.images)
          .pipe(($.imagemin({
              optimizationLevel: 3,
              progressive: true,
              interlaced: true
          })))
          .pipe(gulp.dest(dist.images));
    });

    gulp.task('html', function () {
        return gulp.src(['app/google4828e13157245221.html'])
          .pipe(gulp.dest(dist.main));
    });

    gulp.task('copy:font', function () {
        return gulp.src(paths.font)
          .pipe(gulp.dest(dist.font));
    });

    gulp.task('copy:icons', function () {
        return gulp.src(paths.icons)
          .pipe(gulp.dest(dist.icons));
    });

    gulp.task('sass', function () {
        return gulp.src(paths.sass)
          .pipe(sass().on('error', sass.logError))
          .pipe(gulp.dest('app/css'));
    });

    gulp.task('clean', function (cb) {
        rimraf('./dist', cb);
    });

    gulp.task('views', function () {
        return gulp.src(paths.views)
          .pipe(gulp.dest(dist.views));
    });

    gulp.task('copy:extra', function () {
        return gulp.src([paths.sitemap, paths.robots])
           .pipe(gulp.dest(dist.main));
    });

    gulp.task('videos', function () {
        return gulp.src([paths.videos])
           .pipe(gulp.dest(dist.videos));
    });

    gulp.task('copy:publishing-profile', function () {
        return gulp.src([paths.profile])
           .pipe(gulp.dest(dist.profile));
    });

    gulp.task('bower', function () {
        return gulp.src(paths.html)
          .pipe(wiredep({
              directory: 'bower_components',
              ignorepaths: '..'
          }))
        .pipe(gulp.dest('app'));
    });

    // Rerun the task when a file changes
    gulp.task('watch', function () {
        //gulp.watch(paths.scripts, ['client:build']);
        gulp.watch(paths.css, ['client:build']);
        gulp.watch(paths.images, ['images']);
        gulp.watch(paths.font, ['copy:font']);
        gulp.watch(paths.icons, ['copy:icons']);
    });

    gulp.task('build', ['clean'], function () {
        runSequence(['images', 'copy:font','views', 'copy:icons', 'client:build', 'html','videos', 'copy:extra', 'copy:publishing-profile']);
    });

    gulp.task('default', ['build']);

})()